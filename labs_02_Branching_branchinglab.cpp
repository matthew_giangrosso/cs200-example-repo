#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

void Program1()
{
    bool done = false;
    while ( !done )
    {
        // Comment for reference
        string name;
        string homeTown;
        cout << "Enter your hometown: ";
        cin >> homeTown;

        if (homeTown.size() > 6)
        {
            cout << "That's a long name!" << endl;
        }

        cout << "Enter your name: ";
        cin >> name;

        cout << "Hello " << name << " from " << homeTown << "!" << endl;


        //Comment for reference
        cout << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}
//Comment for reference
void Program2()
{
    float grade;
    float userPoints;
    float totalPoints;

    cout << "How many points total? ";
    cin >> totalPoints;

    cout << "How many points did you get? ";
    cin >> userPoints;

    grade = (userPoints / totalPoints);

    cout << "Score: " << grade << endl;

    if (grade >= .60)
    {
        cout << "You passed!";
    }
    else
    {
        cout << "You failed!";
    }
}

void Program3()
{
    int charge;
    cout << "Enter your phone charge: ";
    cin >> charge;

    if (charge >= 75)
    {
        cout << "[****]";
    }
    else if (charge >= 50)
    {
        cout << "[***_]";
    }
    else if (charge >= 25)
    {
        cout << "[**__]";
    }
    else if (charge >= 5)
    {
        cout << "[*___]";
    }
    else
    {
        cout << "[____]";
    }
}

void Program4()
{
    int userChoice;

    cout << "What is your favorite type of book?" << endl << "1. Scifi" << endl << "2. Historical" << endl
        << "3. Fantasy" << endl << "4. DIY" << endl << endl << "Your selection: ";

    cin >> userChoice;

    if (userChoice >= 1 && userChoice <= 4)
    {
        cout << "Good choice!";
    }
    else
    {
        cout << "Invalid choice!";
    }
}

void Program5()
{
    float num1;
    float num2;
    float result;
    char mathOperator;

    cout << "Enter first number:  ";
    cin >> num1;

    cout << "Enter second number: ";
    cin >> num2;

    cout << "What type of operation? " << endl << "+: add" << endl << "-: subtract" << endl <<
        "*: multiply" << endl << "/: divide" << endl << endl <<  "Choice: ";
    cin >> mathOperator;

    switch ( mathOperator )
    {
    case '+':
        result = num1 + num2;
        break; 

    case '-':
            result = num1 - num2;
            break;

    case '*':
            result = num1 * num2;
            break;

    case '/':
        if (num2 == 0)
        {
            cout << "Can't divide by 0!" << endl;
            result = 0;
            break;
        }
            result = num1 / num2;
            break;

    default:
        cout << "Invalid Operation";
        result = 0;
        break;
    }
    cout << endl << "Result: " << result;
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
